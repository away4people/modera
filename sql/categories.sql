SET NAMES 'utf8';

DROP TABLE IF EXISTS categories;
CREATE TABLE categories (
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  category VARCHAR(255) NOT NULL,
  parent_id INT(10) UNSIGNED NOT NULL,
  number INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci;

INSERT INTO categories VALUES
(1, 'Ноутбуки', 0, 1),
(2, 'Acer', 1, 1),
(3, 'Lenovo', 1, 2),
(4, 'Apple', 1, 3),
(5, 'Macbook Air', 4, 1),
(6, 'Macbook Pro', 4, 2),
(7, 'Sony Vaio', 1, 4),
(8, 'Смартфоны', 0, 2),
(9, 'iPhone', 8, 1),
(10, 'Samsung', 8, 2),
(11, 'LG', 8, 3),
(12, 'Vertu', 8, 4),
(13, 'Комплектующие', 0, 3),
(14, 'Процессоры', 13, 1),
(15, 'Память', 13, 2),
(16, 'Видеокарты', 13, 3),
(17, 'Жесткие диски', 13, 4);